const { exchangeRates } = require('../src/util.js');

console.log(exchangeRates)

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})


	app.post('/currency', (req, res) => {


		const names = Object.values(exchangeRates).map(currency => currency.name)
		const aliases = Object.values(exchangeRates).map(currency => currency.alias);
		const exchanges = Object.values(exchangeRates).map(currency => currency.ex);

		if(!names.includes(req.body.name) && !aliases.includes(req.body.alias) && !exchanges.includes(req.body.ex)) {
			//NAME

			if(!req.body.hasOwnProperty('name')){
				return res.status(400).send({
					'error': 'Bad Request - missing required parameter NAME'
				})
			}
			if(typeof req.body.name !== 'string'){
				return res.status(400).send({
					'error': 'Bad Request - NAME has to be a string'
				})
			}
			if(req.body.name === ""){
				return res.status(400).send({
					'error': 'Bad Request - NAME should not be empty'
				})
			}

			//EX
			if(!req.body.hasOwnProperty('ex')){
				return res.status(400).send({
					'error': 'Bad Request - missing required parameter EX'
				})
			}

			if(typeof req.body.ex !== 'object'){
				return res.status(400).send({
					'error': 'Bad Request - EX has to be a object'
				})
			}

			if(Object.keys(req.body.ex).length === 0){
				return res.status(400).send({
					'error': 'Bad Request - EX should not be empty'
				})
			}

			//ALIAS
			if(!req.body.hasOwnProperty('alias')){
				return res.status(400).send({
					'error': 'Bad Request - missing required parameter ALIAS'
				})
			}
			if(typeof req.body.alias !== 'string'){
				return res.status(400).send({
					'error': 'Bad Request - ALIAS has to be a string'
				})
			}
			if(req.body.alias === ""){
				return res.status(400).send({
					'error': 'Bad Request - ALIAS should not be empty'
				})
			}

			// Complete input is given and no duplicates
			return res.send({
				currency: req.body
			});
		} 

		return res.status(400).send({
			'error': 'Bad Request - Dupliate Currency'
		})


    })
}
