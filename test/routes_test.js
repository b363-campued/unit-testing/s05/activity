const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	// it('test_api_get_rates_returns_object_of_size_5', (done) => {
	// 	chai.request('http://localhost:5001')
	// 	.get('/rates')
	// 	.end((err, res) => {
	// 		expect(Object.keys(res.body.rates)).does.have.length(5);
	// 		done();	
	// 	})		
	// })

	//COMPLETE INPUT
	it('test_api_post_currency_returns_200_if_complete_input_is_given', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        })
    })

	//NAME
	it('test_api_post_currency_returns_400_if_no_name', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_name_is_not_string', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 500,
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

	it('test_api_post_currency_returns_400_if_name_is_empty', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': '',
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

	//EX
	it('test_api_post_currency_returns_400_if_no_ex', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_ex_is_not_object', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': 'String',
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

	it('test_api_post_currency_returns_400_if_ex_is_empty', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': {},
			'alias': 'CAD'
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })


	//ALIAS
	it('test_api_post_currency_returns_400_if_no_alias', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			}
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

    it('test_api_post_currency_returns_400_if_alias_is_not_string', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': 500
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

	it('test_api_post_currency_returns_400_if_alias_is_empty', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': ''
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

	//Duplicates
	it('test_api_post_currency_returns_400_if_duplicate_alias', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Chinese Yuan',
            'alias': 'CNY',
            'ex': {
                'peso': 7.21,
                'usd': 0.14,
                'won': 168.85,
                'yen': 15.45
            }
        })
        .end((err, res) => {
            expect(res.status).to.equal(400);
            done();
        })
    })

	it('test_api_post_currency_returns_200_if_no_dupes', (done)=> {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
            'name': 'Canadian Dollar',
			'ex': {
				'peso': 47.32,
				'usd': 0.73,
				'won': 984.30,
				'yuan': 5.25
			},
			'alias': 'CAD'
                
        })
        .end((err, res) => {
            expect(res.status).to.equal(200);
            done();
        })
    })
	
})
